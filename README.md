#Basics for Setting up the Omega UA Subtheme Lambda

##Basic Install of the Theme Files

Install the **latest 3.x version** of the Omega theme from <http://drupal.org/project/omega> into your site’s theme directory (**do not enable** the theme). Next, upload the UA sub-theme, and enable it in your site’s Appearance section (URL/admin/appearance).

##Useful—if not indispensable—modules

* To save your sanity and the integrity of the theme in Internet Explorer, download the **Conditional Stylesheets** module at <http://drupal.org/project/conditional_styles> (conditional stylesheet included in Lambda).
* If you want to take the site’s logo, title, etc. out of the branding zone, download and enable the **Delta** module from <http://drupal.org/project/delta>
* If you want make different themes/layouts for particular situations, download and enable the **Context** module from <http://drupal.org/project/context>.
* If you would like to alter the CSS styles of the theme, but don’t want to alter the theme itself, I suggest installing the **CSS Injector** module from <http://drupal.org/project/css_injector>.
* If you want to export theme settings, make a sub-theme and aren’t fluent with the Drupal file system or Drush, I suggest you use **Omega Tools** at <http://drupal.org/project/omega_tools> for development (requires CTools).
* To make HTLM5 and CSS3 backwards compatible with Internet Explorer 6+, install **Modernizr** <http://drupal.org/project/modernizr> but remember: do not sacrifice accessibility for convenience or performance!

##Appearance Settings – vertical left tabs
Click the Settings link underneath the theme title and description.

###Grid Settings
* Set the grid system to 960px (unless you plan to make a completely fluid site).
* Under responsive settings, check “Enable the responsive grid,” and set all the Viewport settings to 1.0.
* Under the default (960px) layout settings—unless you are planning on displaying a fluid, narrow or wide layout—
make sure the Fluid, Narrow and Wide boxes are unchecked.

###Zone and region configuration
####General settings
In order to give the main areas of content in the website a background, expand the zone you want to have a background and add one of these classes to it:

Class name           | Effect                                                 |              |
-------------------- | ------------------------------------------------------ | ------------ |
white-bkg            | gives the element a white background (#ffffff)         |              |
uared-bkg            | UA official red background #ab0520                     |              |
uablue-bkg           | UA official blue background #002147                    |              |
charcoal-bkg         | UA ThinkTank color background #363636                  |              |
tucson-tan1-bkg      | UA official extended colors background #e5c19f         |              |
tucson-tan2-bkg      | UA official extended colors background #c88a11         |              |
golden-sun-bkg       | UA official extended colors background #ffd18b         |              |
town-brown-bkg       | UA official extended colors background #572700         |              |
sunset-violet-bkg    | UA official extended colors background #781d7e         |              |
rose-violet-bkg      | UA official extended colors background #d492b3         |              |
squash-blossom-bkg   | UA official extended colors background #008c7f         |              |
succulent-green-bkg  | UA official extended colors background #93a445         |              |
tan-bkg              | Common UA tan background color #efebdb                 |              |
lighter-tan-bkg      | Common UA website tan/beige color background #f7f6ee   |              |
tan-to-lighter       | Tan gradating down to lighter tan (#efebdb to # f8f8f3)|              |
blue-to-tan          | Blue gradating down to tan (#012d5d to # e9e7d8)       |              |
tan-to-blue          | Tan gradating down to Blue (#e9e7d8 to #012d5d)        |              |
red-to-tan           | Red gradating down to tan (#ab0520 to # e9e7d8)        |              |
tan-to-red           | Tan gradating down to Red (#e9e7d8 to #ab0520)         |              |
no-bkg               | makes the background transparent                       |              |

####Other CSS Classes
There are also other classes available in the global.css file that may be convenient:

| Class name                      | Effect                                                                                                              |
| ------------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| span.clear                      | a block-level span element which will clear all other block-level elements                                          |
| clear                           | class used to clear both right and left floating elements                                                           |
| clear-block                     | class for making an element behave as if it were a block-level element                                              |
| clear-none                      | class for making an element not clear floating elements                                                             |
| clearfix:after, container:after | adds a single space that doesn’t take up any room after the elements with classes of clearfix and container         |
| clearfix, container             | makes elements behave as if they were block-level elements                                                          |
| float-left                      | makes a block-level element float left                                                                              |
| float-right                     | makes a block-level element float right                                                                             |
| img-left                        | makes an image container float left of adjacent content and adds a 2 em space between it and the content            |
| img-right                       | makes an image container float right of adjacent content and adds 2 em space between it and the content             |
| first                           | zeros out any default margins and paddings attached to this class                                                   |
| last                            | zeros out any default margins and paddings attached to this class                                                   | 
| top                             | zeros out any default margins and paddings attached to this class                                                   |
| bottom                          | zeros out any default margins and paddings attached to this class                                                   |
| text-left                       | aligns text left                                                                                                    |
| text-right                      | aligns text left                                                                                                    |
| text-center                     | centers text                                                                                                        |
| text-justify                    | aligns text to be justified                                                                                         |
| bold                            | makes a font’s weight bold                                                                                          |
| italic                          | puts text in italics                                                                                                |
| underline                       | underlines text                                                                                                     |
| highlight                       | highlights text with #ff0 (yellow)                                                                                  |
| quiet                           | dims the text to (#666) dark gray                                                                                   |
| loud                            | makes text completely black                                                                                         |
| added                           | gives text a green background with white text                                                                       |
| removed                         | gives text a red background with white text                                                                         |
| small                           | shrinks text to be 0.9167 em                                                                                        |
| large                           | inflates text to 1.2 em                                                                                             |
| nopadding                       | zeros out padding                                                                                                   |
| noindent                        | zeros out left padding and margin                                                                                   |             
| nobullet                        | takes away list bullet points                                                                                       |
| normalcase                      | removes all text transform properties                                                                               |
| uppercase                       | makes all letters capitalized                                                                                       |
| no-embellish, no-embellish li   | takes all borders, list images, list styles, padding and margin off unordered list elements                         |

*If your site does not require a UA logo at the top, ignore the next TWO bullet points.*

* In the Header section, make sure the Branding is at the top of the list. If it is not, click the zone’s configuration link and choose the lowest number in the Weight dropdown, the small the weight the lighter it is, light things float up just like in nature.
* Expand the options for the Branding zone by clicking its Configuration link, and then check the box that says, “Force this zone to be rendered.”
* Standard enabled areas:
  * Header Section
    *  Branding zone (12 cols)
       *  Regions: Branding (12 cols)
    *  Header zone (12 cols)
       *  Regions: Header first (12 cols)
    *  Menu zone (12 cols)
       *  Regions: Menu (12 cols)
   * Content Section
      *  Content zone (24 cols)
         * Regions: Content (16 cols), Sidebar second (8 cols)
   *  Footer Section
      * Footer zone
         * Regions: Footer first (1 col)*, Footer second (12cols)
 
      
###Debugging
Only used for debugging layout and visualizing all content regions filled.

###Toggle libraries
Make sure to check the boxes for Formalize and Media Queries.

###Toggle styles
You can turn off or turn on styles for particular aspects of the site to override styles more easily. The theme’s default styling for the navigation bar is located here—uncheck to override.

###Toggle advanced elements
You can turn parts of the site off instead of manually altering template files.

****
##Appearance Settings – below vertical tabs
###Nice Menu (if enabled) settings
path_to_your_site/themes/nu_ua/css/nicer-menus.css
Note: If you use Nice Menus, you need to define their background colors in the nicer-menus.css stylesheet or make a more specific declaration with CSS Injector.
###Toggle Display
In these settings it is advised to turn **off** the logo, site name, site slogan and secondary menu and use the Delta block settings (located at **Base_URL/admin/config/user-interface/delta-blocks**) for these pieces instead.

**Note:** The Secondary menu is output in an undesireable spot in this theme’s layout. If you wish to use the secondary menu, I suggest using the **Menu Block** module <http://drupal.org/project/menu_block> to position the secondary menu anywhere in the layout.

##Further Support

###Omega
  *  **Omega theme informational micro-site** <http://omega.developmentgeeks.com>
  *  **Omega Handbook** <http://drupal.org/node/819164>
  *  **Omega Framework Group** <http://groups.drupal.org/omega-framework>
  *  **Omega support IRC chat room** #drupal-omega
  
###UA Omega Subtheme: Lambda

  *  Rebecca L. Macaulay,
     Student Affairs Systems Group (SASG) <macaulay@email.arizona.edu>
     Bdg 409a
     520.626.0899

###Mobile and Responsive Web
  *  **Better Jump Menus** [http://drupal.org/project/jump_menu](http://)
  *  **Mobile Guide for Drupal** [http://drupal.org/documentation/mobile](http://)
  *  **Mobile First** [http://www.abookapart.com/products/mobile-first](http://)
  *  **Responsive Web Design** [http://www.abookapart.com/products/responsive-web-design](http://)







￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼
   
  


  
      




















￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼
￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼


