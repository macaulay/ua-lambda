<?php
function lambda_form_system_theme_settings_alter(&$form, $form_state) { /** alter theme settings form **/

	// Import Theme-setting specific CSS
	drupal_add_css(drupal_get_path('theme', 'lambda') . '/css/lambda-theme-settings.css', array('group' => CSS_THEME, 'weight' => 100)); 

 /*
  * Create the UA Logo form using Forms API *
  */
  $form['uabrand'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('UA Branding settings'),
  );
	
  // Define 'Choose UA Logo Color': 
  $form['uabrand']['ua_banner_links'] = array(
    '#type'          => 'select',
    '#options'       => array(
      'yes'          => 'Yes',
      'no'           => 'No'),
    '#default_value' => theme_get_setting('ua_banner_links'),
    '#title'         => t('You probably want the links but you are not required to use them'),   
   );
	 
  // Define 'Choose a UA Banner Color':
  $form['uabrand']['uabanner'] = array(
    '#type'             	=> 'select',
    '#options'          	=> array(
      'red-42'           	=> 'Red 42px',
      'red-grad-42'         => 'Red gradient 42px',
      'blue-42'             => 'Blue 42px',  
      'blue-grad-42'        => 'Blue gradient 42px',
      'light-gray-42'       => 'Light gray 42px',  
      'light-gray-grad-42'  => 'Light gray gradient 42px',
      'dark-gray-42'        => 'Dark gray 42px',  
      'dark-gray-grad-42'   => 'Dark gray gradient 42px',
      'white-42'            => 'White 42px',
      'charcoal-42'         => 'Charcoal 42px',
      'charcoal-grad-42'    => 'Charcoal gradient 42px',
      'red-25'           	=> 'Red 25px',
      'red-grad-25'         => 'Red gradient 25px',
      'blue-25'             => 'Blue 25px',  
      'blue-grad-25'        => 'Blue gradient 25px',
      'light-gray-25'       => 'Light gray 25px',  
      'light-gray-grad-25'  => 'Light gray gradient 25px',
      'dark-gray-25'        => 'Dark gray 25px',  
      'dark-gray-grad-25'   => 'Dark gray gradient 25px',
      'white-25'            => 'White 25px',
      'charcoal-25'         => 'Charcoal 25px',
      'charcoal-grad-25'    => 'Charcoal gradient 25px',
),
    '#default_value' => theme_get_setting('uabanner'),
    '#title'         => t('Choose a banner style for your site'),
    '#description'   => t('<p><strong>Important:</strong> You are only allowed to use the 25px high banner if you have an <em>approved</em> UA Sidecar logo (example shown below). To obtain an approved sidecar logo for your organization, contact Leslie Johnston, UA Marketing Creative Director, at lesliej@email.arizona.edu.</p><img src="//redbar.arizona.edu/sites/default/files/linelogowsidecar_0.jpg" title="Example of UA Sidecar logo with 25px banner" alt="25px banner above a logo with the UA Block A">')
);
  
	// Define 'Choose a Main Background Class':
  $form['uabrand']['uabackground'] = array(
    '#type' => 'textfield',
    '#title' => t('The Class for the Main Background Color or Gradient'),
    '#default_value' => theme_get_setting('uabackground'),
    '#size' => 12,
    '#maxlength' => 60,
    '#required' => FALSE,
    '#description' => t('<p><strong>Class must be defined in the <span style="text-decoration:underline;" title="located on server in sites/-siteurl or all-/themes/lambda/css/global.css">global.css</span> file or in a <a href="/admin/config/development/css-injector" title="Add styles to CSS">CSS Injector rule</a>.</strong> Defaults to <strong>blue-to-tan</strong>.</p><p><strong>Already available classes include:</strong></p><p>Gradients:</p><ul><li>blue-to-tan</li><li>tan-to-blue</li><li>red-to-tan</li><li>tan-to-red</li><li>tan-to-white</li><li>white-to-tan</li><li>lighter-to-tan</li><li>tan-to-lighter</li></ul><p>Solid colors:</p><ul><li>white-bkg</li><li>uablue-bkg</li><li>uared-bkg</li><li>charcoal-bkg</li><li>tucson-tan1-bkg</li><li>tucson-tan2-bkg</li><li>golden-sun-bkg</li><li>town-brown-bkg</li><li>sunset-violet-bkg</li><li>rose-violet-bkg</li><li>squash-blossom-bkg</li><li>succulent-green-bkg</li><li>tan-bkg</li><li>lighter-tan-bkg</li><li>no-bkg</li></ul>')
	);
  
}//form alter function end