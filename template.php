<?php
/**
 * Contains theme override functions and preprocess functions for the UA-specific theme.
 **/

/**
 * Implements hook_theme().
 */
function lambda_theme(&$existing, $type, $theme, $path) {
  $hooks = array();
  return $hooks;
}
 
// Modifies html output, this is where you can add things into the <head> tag
function lambda_preprocess_html(&$variables) {   
    drupal_add_css('//redbar.arizona.edu/sites/default/files/ua-banner/ua-web-branding/css/ua-web-branding.css', array('type' => 'external'));
    drupal_add_js('//redbar.arizona.edu/sites/default/files/ua-banner/ua-web-branding/js/ua-web-branding.js', 'external');
    $variables['attributes_array']['class'][] = theme_get_setting('uabackground');
} 
 
// Prints the UA banner links
function lambda_ua_banner_links() {
  $ua_banner_links = theme_get_setting('ua_banner_links');
  switch($ua_banner_links) {
    case ("yes"):
      $links = '<div class="ua-home icon-list rsvp"></div>
    <ul class="ua-home tldr ua-menu open">
      <li class="ua-search"><a class="icon-search" href="http://www.arizona.edu/search/google">UA Search</a></li>
      <li class="ua-phonebook"><a class="icon-phone" href="http://www.arizona.edu/phonebook">UA Phonebook</a></li>
      <li class="ua-calendar"><a class="icon-calendar" href="http://uanews.org/calendar/day">UA Calendar/Events</a></li>
      <li class="ua-map"><a class="icon-map" href="http://www.arizona.edu/maps">UA Campus Map</a></li>
    </ul>';
      break;
    case ("no"):
      $links = '<div></div>';
      break;
  }
  if (isset($links)) {
    return $links;
  }
   if (empty($ua_banner_links)) {
      $ua_banner_links = 'none';
    }
  return $ua_banner_links;
}

// Sets the UA banner background color
function lambda_uabanner() {
  $uabanner = theme_get_setting('uabanner');
  switch($uabanner) {
    case ("red-42"):
      $class = 'ua-wrapper bgDark red';
      break;
    case ("red-grad-42"):
      $class = 'ua-wrapper bgDark red-grad';
      break;
    case ("blue-42"):
      $class = 'ua-wrapper bgDark blue';
      break;
    case ("blue-grad-42"):
      $class = 'ua-wrapper bgDark blue-grad';
      break;
    case ("light-gray-42"):
      $class = 'ua-wrapper bgLight light-gray';
      break;
    case ("light-gray-grad-42"):
      $class = 'ua-wrapper bgLight light-gray-grad';
      break;
    case ("dark-gray-42"):
      $class = 'ua-wrapper bgDark dark-gray';
      break;
    case ("dark-gray-grad-42"):
      $class = 'ua-wrapper bgLight dark-gray-grad';
      break;
    case ("white-42"):
      $class = 'ua-wrapper bgLight white';
      break;
    case ("charcoal-42"):
      $class = 'ua-wrapper bgDark black';
      break;
    case ("charcoal-grad-42"):
      $class = 'ua-wrapper bgDark black-grad';
      break;
    case ("red-25"):
      $class = 'ua-wrapper bgDark red twenty-five';
      break;
    case ("red-grad-25"):
      $class = 'ua-wrapper bgDark red-grad twenty-five';
      break;
    case ("blue-25"):
      $class = 'ua-wrapper bgDark blue twenty-five';
      break;
    case ("blue-grad-25"):
      $class = 'ua-wrapper bgDark blue-grad twenty-five';
      break;
    case ("light-gray-25"):
      $class = 'ua-wrapper bgLight light-gray twenty-five';
      break;
    case ("light-gray-grad-25"):
      $class = 'ua-wrapper bgLight light-gray-grad twenty-five';
      break;
    case ("dark-gray-25"):
      $class = 'ua-wrapper bgDark dark-gray twenty-five';
      break;
    case ("dark-gray-grad-25"):
      $class = 'ua-wrapper bgLight dark-gray-grad twenty-five';
      break;
    case ("white-25"):
      $class = 'ua-wrapper bgLight white twenty-five';
      break;
    case ("charcoal-25"):
      $class = 'ua-wrapper bgDark black twenty-five';
      break;
    case ("charcoal-grad-25"):
      $class = 'ua-wrapper bgDark black-grad twenty-five';
      break;
  }
  if (isset($class)) {
    return ' class="'. $class .'"';
  }
   if (empty($uabanner)) {
      $uabanner = 'none';
    }
    return $uabanner;
}
     
// Sets the variables for the two above functions
function lambda_preprocess_region(&$variables) {
	if ($variables['region'] == 'branding') {
			$variables['lambda_ua_banner_links'] = lambda_ua_banner_links();
			$variables['lambda_uabanner'] = lambda_uabanner();
		 }
}

// Return a themed Search Form ***
function lambda_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#title'] = t('Enter the terms you wish to search for.'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
    $form['search_block_form']['#size'] = 15;  // define size of the textfield
    $form['search_block_form']['#default_value'] = t(''); // Set a default value for the textfield
    $form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search.png');//switch file if needed
        
   // Add extra attributes to the text box
   // $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search';}";
   // $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search') {this.value = '';}";
  }    
}
